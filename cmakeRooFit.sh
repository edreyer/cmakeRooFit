#Semi-useful links:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/RootCoreToCMake
# https://root.cern.ch/how/integrate-root-my-project-cmake

#Make the directory where everything lives
mkdir MakeMinimalC
cd MakeMinimalC
mkdir source build run

#AnalysisBase is needed for the initial CMakeLists.txt
setupATLAS
asetup AnalysisBase,2.6.2,here

cp CMakeLists.txt source
cd source
mkdir HelloWorld
cd HelloWorld

#Write a super simple macro which requires only the TString header from ROOT
echo "#include <iostream>" >> helloworld.cxx
echo "#include \"TString.h\"" >> helloworld.cxx
echo "#include \"TCanvas.h\"" >> helloworld.cxx
echo "#include \"RooAbsReal.h\"" >> helloworld.cxx
echo "#include \"RooRealVar.h\"" >> helloworld.cxx
echo "int main() {" >> helloworld.cxx
echo "TString message = \"Hello world! \";" >> helloworld.cxx
echo "TCanvas* canvas = new TCanvas(\"helloWorld\",\"helloWorld\",800,600);" >> helloworld.cxx
echo "canvas->Print(\"helloWorld.pdf\",\"pdf\");" >> helloworld.cxx
echo "RooRealVar *x = new RooRealVar(\"x\",\"x\",1.,0.,10.);" >> helloworld.cxx
echo "std::cout << message << x->getVal() << std::endl;" >> helloworld.cxx
echo "return 0;" >> helloworld.cxx
echo "}" >> helloworld.cxx

#Write a CMakeLists.txt file to compile this macro
echo "cmake_minimum_required( VERSION 3.2 )" >> CMakeLists.txt
echo "project( HelloWorld )" >> CMakeLists.txt
echo "find_package( ROOT REQUIRED COMPONENTS RIO Net RooFit RooFitCore Graf Gpad Hist Tree )" >> CMakeLists.txt
echo "include_directories( \${ROOT_INCLUDE_DIRS} )" >> CMakeLists.txt
echo "link_libraries( \${ROOT_LIBRARIES} \${ROOT_ROOFIT_LIBRARY} )" >> CMakeLists.txt
echo "add_executable( HelloWorld helloworld.cxx )" >> CMakeLists.txt

#Build the HelloWorld project
cd ../../build
cmake ../source
make

#Run the binary!
./x86_64-slc6-gcc49-opt/bin/HelloWorld
